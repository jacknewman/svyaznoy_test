"use strict";

var ModelProvider = (function() {
  /** @private */
  var _routes = {
    mosmetro: '../json/mosmetro.json'
  }

  /**
   * @param {string} link : address to file
   * @param {function} onrequest : return on nonload states
   * @param {function} callback : return on response
 */
  function _getRequest(link, callback){
    var $http = new XMLHttpRequest();
    $http.open('GET', link);
    $http.send();
    $http.onreadystatechange = function(e) {
      return $http.readyState == 4 ? callback(JSON.parse($http.response)) : true;
    }
  }

  /** @public */
  return {
    getStorage: function(r, callback) {
      var route = _routes[r];
      if (route){
        _getRequest(route, callback);
      } else {
        console.warn('Data not found')
      }
    }
  }
})()
