"use strict";

var TableManager = (function() {
  var _store = {};

  /**
   * @param {array} arrObj : input array of object
   * @param {string} f : field to sort
   * @param {string} p : sort direction
   * @return {array} arrObj
   */
  function _sortItems(arrObj, f, p){
    arrObj.sort(function(a,b) {
      if (a[f]<b[f]){
        return p && p == '-' ? 1 : -1;
      } else if (a[f]>b[f]){
        return p && p == '-' ? -1 : 1;
      } else {
        return 0;
      }
    });
    return arrObj;
  }

  /** @public */
  return {
      findItems: function(query, type, field, sort){
        var filteredStore = {}, s=[];
        query = query.toLowerCase();
        for (var i=0; i<_store[type].length; i++){
          var current = _store[type][i][field].toLowerCase();
          if (current && current.match(query) && current.match(query).length>0) s.push(_store[type][i])
        }
        return _sortItems(s,field,sort);
      },
      fetchData: function(response) {
        _store = (JSON.parse(JSON.stringify(response)));
        response.stations = _sortItems(response.stations,'name','+');
        TableView.fillRows(response);
      },
      sortItems: _sortItems
    }
})()
