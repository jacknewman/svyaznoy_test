"use strict";
var TableView = (function() {
  var _stateStore = {},
      _listClasses = {
        branches: 'jsBranchesList',
        stations: 'jsStationsList'
      };

  document.addEventListener('DOMContentLoaded', function(e) {
    ModelProvider.getStorage('mosmetro', TableManager.fetchData);
  });

  document.getElementById('jsSortColumn').addEventListener('click', function() {
    var sortField = this.getAttribute('data-sort-field'),
        value = this.getAttribute('data-value'),
        valueAfter = value == '-' ? '+' : '-',
        sortArray = this.getAttribute('data-sort');
    _stateStore[sortArray] = TableManager.sortItems(_stateStore[sortArray], sortField, valueAfter);
    _updateRenderRows('stations','jsStationsList');
    this.parentNode.classList.toggle('inverse');
    this.setAttribute('data-value', valueAfter)
  });

  document.getElementById('searchField').addEventListener('keyup', function() {
    var curSort = _checkSort();
    _stateStore['stations'] = TableManager.findItems(this.value, 'stations', 'name', curSort);
    _fillRows(_stateStore, 'stations');
  });

  function _checkSort() {
    return document.getElementById('jsSortColumn').getAttribute('data-value');
  }

  function _createTemplate() {
    var t = document.createElement('div');
    t.className = 'table-container_item_content_row';
    return t;
  }

  function _updateRenderRows(type, dom) {
    var domBox = document.getElementsByClassName(dom);

    for (var j=0; j<domBox.length; j++){
      var item = domBox[j].getAttribute('data-field');

      for (var i=0; i<_stateStore[type].length; i++){
          domBox[j].childNodes[i].innerHTML = _stateStore[type][i][item];
      }
    }
  }

  /**
   * @param {string} type (optional) : type of array at store to render part of view
   * @param {object} store : full store to render view
 */
  function _fillRows(store, t) {
    _stateStore = store;

    if (t && _stateStore.hasOwnProperty(t)){
      changeView(t);
    } else {
      for (var type in _stateStore){
        if (_stateStore.hasOwnProperty(type)){
          changeView(type);
        }
      }
    }

    function changeView(type) {
      var domBox = document.getElementsByClassName(_listClasses[type]);
      for (var j=0; j<domBox.length; j++){
        var item = domBox[j].getAttribute('data-field');
        domBox[j].innerHTML = '';
        for (var i=0; i<_stateStore[type].length; i++){
            var template = _createTemplate();

            template.innerHTML = _stateStore[type][i][item];
            domBox[j].appendChild(template);
        }
      }
    }
  }

  /* @public */
  return {
    fillRows: _fillRows
  }
})()
