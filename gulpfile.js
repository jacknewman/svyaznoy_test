"use strict";

var gulp = require('gulp'),
    connect = require('gulp-connect'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    minifyCss = require('gulp-minify-css'),
    jade = require('gulp-jade');

// connect server
gulp.task('connect', function() {
    console.log('Server started at: ');
    connect.server({
        root: './',
        livereload: true
    });
});

//less
gulp.task('sass', function() {
    gulp.src('./css/common.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(prefix({
            browsers: ['> 1%', 'IE 10'],
            cascade: true,
            remove: false
        }))
        .pipe(minifyCss({
            compatibility: 'ie10',
            keepSpecialComments: 0,
            keepBreaks: true
        }))
        .pipe(gulp.dest('./css'))
        .pipe(connect.reload());
});

gulp.task('templates', function() {
   gulp.src('./template/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('./'))
        .pipe(connect.reload());
});

gulp.task('reload', function() {
  gulp.src('./').pipe(connect.reload());
})

//watch
gulp.task('watch', function() {
    gulp.watch(['./css/*.scss'], ['sass']);
    gulp.watch(['./template/*.jade'], ['templates']);
    gulp.watch(['./js/*.js'], ['reload']);
});

//default
gulp.task('default', ['connect', 'templates', 'sass', 'watch']);
